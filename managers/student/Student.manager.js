const { SchemaTypes, model, Types } = require('mongoose');


module.exports = class StudentManager {

  constructor({ utils, cache, config, cortex, managers, validators, mongomodels } = {}) {
    this.config = config;
    this.validators = validators;
    this.mongomodels = mongomodels;
    this.managers = managers;

    this.httpExposed = ['create', 'get=find'];
  }

  async create({__longToken,__authorizeSchoolAdminOnSchool, __authorizeSchoolAdminOnClassroom, name, email, password, status, code, grade, schoolId, classroomId }) {
    const studentData = { name, email, password, status, code, grade, schoolId, classroomId };

    // Data validation
    let result = await this.validators.student.create(studentData);
    if (result) return result;


    try {

      const school = await this.mongomodels.school.findById(schoolId);
      console.log(school)
      if (!school) throw Error('School not found');

      const classroom = await this.mongomodels.classroom.findById(classroomId);
      if (!classroom) throw Error('Classroom not found');


      const student = await this.mongomodels.student.create(studentData);
      return {
        student,
      }
    } catch (error) {
      return {
        error
      }
    }
  }

  async find({ __longToken, __query }) {
    let students;
    if (__query) {
      students = await this.mongomodels.student.find({ ...__query });
    } else {
      students = await this.mongomodels.student.find();
    }
    return {
      students
    };
  }


}