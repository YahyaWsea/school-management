

module.exports = {
  create: [
    {
      model: 'name',
      required: true
    },
    {
      model: 'slug',
      required: true
    },
    {
      model: 'schoolId',
      required: true
    },
  ],
  find: [
    {
      model: 'name',
      required: true
    },
    {
      model: 'slug',
      required: true
    }
  ]
}


