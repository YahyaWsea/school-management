module.exports = ({ meta, config, managers, mongomodels }) =>{
  return async ({ req, res, next })=>{
    const { userKey } = req.decoded;
    const { schoolId }= req.body;

    if (userKey === 'superadmin') {
      return managers.responseDispatcher.dispatch(res, {ok: false, code:401, errors: 'You are not school admin'});
    }

    const school = await mongomodels.school.findById(schoolId);

    
    const hasAccess = school && school.name === userKey;
    if (!hasAccess) {
      return managers.responseDispatcher.dispatch(res, {ok: false, code:401, errors: 'You dont have access on this school'});
    }
    
    next()
  }
}