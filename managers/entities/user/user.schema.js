

module.exports = {
    login: [
        {
            model: 'username',
            required: true,
        },
        {
            model: 'password',
            required: true
        },
        {
            model: 'principalType',
            required: true
        }
    ],
}


