const jwt = require('jsonwebtoken');
const { nanoid } = require('nanoid');
const md5 = require('md5');


module.exports = class UserManager {

  constructor({ config, managers, mongomodels, validators }) {
    this.config = config;
    this.tokenManager = managers.token;
    this.responseDispatcher = managers.responseDispatcher;
    this.validators = validators;
    this.mongomodels = mongomodels;
    this.httpExposed = ['login'];
  }


  /** generate shortId based on a longId */
  async login({ __login, res, username, password, principalType }) {

    let result = await this.validators.user.login({ username, password, principalType });
    if (result) return result;
    let longToken = this.tokenManager.genLongToken({ userId: username, userKey: principalType });

    return { token: longToken };
  }
}