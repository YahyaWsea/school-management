const { SchemaType } = require('mongoose');
module.exports = ({ meta, config, managers, mongomodels }) => {
  return async ({ req, res, next }) => {
    const { userKey } = req.decoded;
    const { schoolId, classroomId } = req.body;

    if (userKey === 'superadmin') {
      return managers.responseDispatcher.dispatch(res, { ok: false, code: 401, errors: 'You are not school admin' });
    }


    const school = await mongomodels.school.findById(schoolId);
    if (!school) {
      return managers.responseDispatcher.dispatch(res, {ok: false, code:401, errors: 'This school is not found'});
    }

    const classroom = await mongomodels.classroom.findById(classroomId);
    console.log({ classroom })
    if (!classroom || classroom.schoolId.toString() !== schoolId) {
      return managers.responseDispatcher.dispatch(res, {ok: false, code:401, errors: 'This classroom is not found'});
    }

    next()
  }
}