

module.exports = {
  create: [
    {
      model: 'name',
      required: true
    },
    {
      model: 'address',
      required: true
    },
    {
      model: 'description',
      required: true
    }
  ],
}


