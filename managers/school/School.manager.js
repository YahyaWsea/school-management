const jwt = require('jsonwebtoken');
const { nanoid } = require('nanoid');
const md5 = require('md5');


module.exports = class SchoolManager {

  constructor({ utils, cache, config, cortex, managers, validators, mongomodels } = {}) {
    this.config      = config;
    this.validators  = validators;
    this.mongomodels = mongomodels;
    this.managers    = managers;

    this.httpExposed = ['create', 'get=find'];
  }

  async create({ __longToken, __authorizeSuperAdmin , name, address, description }) {
    const schoolData = { name, address, description };

    // Data validation
    let result = await this.validators.school.create(schoolData);
    if (result) return result;

    try {
      const school = await this.mongomodels.school.create(schoolData);
      
      return {
        school,
      }
    } catch (error) {
      return {
        error
      }
    }

  }

  async find({ __longToken, __authorizeSuperAdmin, __query, res }) {
    let school;
    if (__query) {
      school = await this.mongomodels.school.find({ ...__query });
    } else {
      school = await this.mongomodels.school.find();
    }
    return {
      school
    };
  }


}