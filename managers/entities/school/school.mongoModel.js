const { Schema, SchemaTypes, model } = require('mongoose');


const SchoolSchema = new Schema(
  {
    name: { 
      type: String,
      unique: true
    },
    description: {
      type: String
    },
    address: {
      type: String,
      required: true
    },
  },
  { timestamps: true },
);

const schoolModel = model('School', SchoolSchema);

module.exports = schoolModel;



