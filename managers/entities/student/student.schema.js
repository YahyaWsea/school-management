

module.exports = {
  create: [
    {
      model: 'name',
      required: true
    },
    {
      model: 'password',
      required: true
    },
    {
      model: 'code',
      required: true
    },
    {
      model: 'email',
    },
    {
      model: 'schoolId',
      required: true
    },
    {
      model: 'classroomId',
      required: true
    },
    {
      model: 'status'
    },
    {
      model: 'grade'
    }
  ],
}


