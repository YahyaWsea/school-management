const { Schema, SchemaTypes, model } = require('mongoose');


const ClassroomSchema = new Schema(
  {
    name: { type: String, required: true },
    slug: {
      type: String,
      required: true,
      unique: true 
    },
    description: {
      type: String
    },
    capacity: {
      type: Number
    },
    schoolId: { type: Schema.Types.ObjectId, ref: 'School' },
  },
  { timestamps: true },
);

const classroomModel = model('Classroom', ClassroomSchema);

module.exports = classroomModel;



