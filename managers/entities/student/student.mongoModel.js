const { Schema, SchemaTypes, model } = require('mongoose');


const StudentSchema = new Schema(
  {
    name: { type: String },
    email: { type: String, unique: true },
    password: { type: String, required: true, hide: true },
    status: {
      type: String,
      enum: ['Pending', 'Active'],
      default: 'Pending',
      hide: true,
    },
    code: {
      type: String,
      unique: true,
    },
    grade: {
      type: String
    },
    classroomId: {
      type: Schema.Types.ObjectId, ref: 'Classroom'
    },
    schoolId: {
      type: Schema.Types.ObjectId, ref: 'School'
    }
  },
  { timestamps: true },
);

const studentModel = model('Student', StudentSchema);

module.exports = studentModel;
