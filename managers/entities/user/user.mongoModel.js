const { Schema, SchemaTypes, model } = require('mongoose');


const UserSchema = new Schema(
  {
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true, hide: true },
    principalType: { type: String, required: true }
  },
  { timestamps: true },
);


const userModel = model('User', UserSchema);

module.exports = userModel;
