<h1  align="center"> School-Management Task </h1>

  

##  Table of Contents

  

- [Introduction](#introduction)
  

- [Tech Stack](#tech-stack)


- [Database Models](#database-models)


- [Deployment](#deployment)


- [Requirements](#requirements)


- [API Doc](#api-doc)


- [Unit Testing](#unit-testing)
  

- [Additional Resources](#additional-resources)


  

##  Introduction

  

   - A school management application that allows users to perform basic CRUD operations on three main entities:
   1 - School,
   2 - Classroom, 
   3 - Student. 

- The application should provide APIs that enable the management of these entities.
- Superadmins will have the ability to add schools.
- School admins can manage classrooms and students within their respective schools.

    ### Important notes regarding current implementation:
	- **We have "superadmin" user (seeded) has access only on School routes and you can login with credentials from ENV variables through /login route**
	- **We have 2 "schoolAdmin" users (seeded) each of them has access on one school with credentials and login like "superadmin"**
	- **schoolAdmins only has access on "Classroom" methods according to their assigned schools.**
	- ***schoolAdmins only could create students in theire assigned schools*
	```
	schoolAdmin1
	{
		username: "schoolAdmin1",
		password: "P@ssw0rd",
		principalType: "sc1" // referes to school(name) that he has access to
	}
	schoolAdmin2
	{
		username: "schoolAdmin2",
		password: "P@ssw0rd",
		principalType: "sc2" // referes to school(name) that he has access to
	}
	```


##  Tech Stack

  - Project is based on Ready boilerplate [qantra-io/axion](https://github.com/qantra-io/axion).

- [Nodejs](https://nodejs.org/) combined with [Expressjs](https://expressjs.com)

- Database Used is [MongoDB](https://www.mongodb.com/) combined with [Mongoose](https://mongoosejs.com/) as ODM.


## Database Models:

![Alt text](docs/school-management-DBModels.png)


##  Deployment

- project is deployed on aws using ec2 service 
- You can find it [here](http://ec2-54-88-24-49.compute-1.amazonaws.com)
- the above domain could possibly be unavailable after a while so don't hestitate to contact me to make it available again




## API Doc
 
- To Be written
- You can find reference through this postman collection link
[Postman Collection Link: Local or Docker](https://api.postman.com/collections/12976739-ad5fce4a-3c66-4b8a-8ae9-ce141622dd48?access_key=PMAT-01HDJ3KMCCTFRSCJQP3B1ZDPP0)

use the following json as environment for Postman collection
```
{
	"id": "4e18333f-526e-4892-95fd-4f9bff47ae71",
	"name": "school-management",
	"values": [
		{
			"key": "devUrl",
			"value": "http://localhost:5111/api",
			"type": "default",
			"enabled": true
		},
		{
			"key": "productionUrl",
			"value": "http://ec2-54-88-24-49.compute-1.amazonaws.com/",
			"type": "default",
			"enabled": true
		}
	],
	"_postman_variable_scope": "environment",
	"_postman_exported_at": "2023-10-25T00:30:01.223Z",
	"_postman_exported_using": "Postman/10.17.2"
}
```




##  Requirements

  

An internet connection of course :).

  

###  Local

  

- Nodejs v18.18.2

- Npm v6.14.8

  

###  Docker

  

- Docker version 19.03.12

- docker-compose version 1.27.1

  

####  Docker Instructions

  

Change directory to the project's root (where `docker-compose.yml` is ) and run the following command which will build the images if the images **do not exist** and starts the containers.

  

When ready, run it:
  

```bash

$ docker-compose up

```


- the server service will run by default on port `5111`, and is accessible from `http://localhost:5111`

- MongoDB will be accessible from `http://localhost:27019`

  

##  Unit Testing
  

- not done yet
- using testing framework called [Mocha](https://mochajs.org/)


  

```bash

$  npm  run  test:unit:mocha

```
