module.exports = ({ meta, config, managers, mongomodels }) =>{
  return async ({req, res, next})=>{
    const { username, password } = req.body;
    const user = await mongomodels.user.findOne({ username, password });
    if (!user) {
      return managers.responseDispatcher.dispatch(res, {ok: false, code:401, errors: 'invalid credentials'});
    }

    next()
  }
}