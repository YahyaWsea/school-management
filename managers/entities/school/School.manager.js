
module.exports = class School {

  constructor({utils, cache, config, cortex, managers, validators, mongomodels }={}){
      this.config              = config;
      this.cortex              = cortex;
      this.validators          = validators; 
      this.mongomodels         = mongomodels;
      this.tokenManager        = managers.token;
      this.schoolsCollection     = "School";
      this.userExposed         = ['create'];
  }

  async create({name, description, address }){
      const user = {username, email, password};

      // Data validation
      let result = await this.validators.user.createUser(user);
      if(result) return result;
      
      // Creation Logic
      // let createdUser     = {username, email, password}
      // let longToken       = this.tokenManager.genLongToken({userId: createdUser._id, userKey: createdUser.key });
      
      // Response
      return {
          user: createdUser, 
          longToken 
      };
  }

}
