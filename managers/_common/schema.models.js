const emojis = require('../../public/emojis.data.json');
const statuses = require('../../public/status.data.json');

module.exports = {
    id: {
        path: 'id',
        type: 'string',
        length: { min: 1, max: 50 },
    },
    name: {
        path: 'name', 
        type: 'string',
        length: {min: 3, max: 20},
    },
    slug: {
        path: 'slug', 
        type: 'string',
        length: {min: 3, max: 20},
    },
    code: {
        path: 'code', 
        type: 'string',
        length: {min: 3, max: 20},
    },
    principalType: {
        path: 'principalType', 
        type: 'string',
    },
    status: {
        path: 'status',
        type: 'string',
        oneOf: statuses.value,
    },
    grade: {
        path: 'grade', 
        type: 'number',
        length: {min: 1, max: 2},
    },
    username: {
        path: 'username',
        type: 'string',
        length: {min: 3, max: 20},
        custom: 'username',
    },
    password: {
        path: 'password',
        type: 'string',
        length: {min: 8, max: 100},
    },
    email: {
        path: 'email',
        type: 'string',
        length: {min:3, max: 100},
    },
    address: {
        path: 'address',
        type: 'string',
        length: {min:3, max: 100},
    },
    title: {
        path: 'title',
        type: 'string',
        length: {min: 3, max: 300}
    },
    label: {
        path: 'label',
        type: 'string',
        length: {min: 3, max: 100}
    },
    description: {
        path: 'description',
        type: 'string',
        length: {min:3, max: 300}
    },
    schoolId: {
        path: 'schoolId',
        type: 'string',
        length: {min:3, max: 100}
    },
    classroomId: {
        path: 'classroomId',
        type: 'string',
        length: {min:3, max: 100}
    },
    shortDesc: {
        path: 'desc',
        type: 'string',
        length: {min:3, max: 300}
    },
    longDesc: {
        path: 'desc',
        type: 'string',
        length: {min:3, max: 2000}
    },
    url: {
        path: 'url',
        type: 'string',
        length: {min: 9, max: 300},
    },
    emoji: {
        path: 'emoji',
        type: 'Array',
        items: {
            type: 'string',
            length: {min: 1, max: 10},
            oneOf: emojis.value,
        }
    },
    price: {
        path: 'price',
        type: 'number',
    },
    avatar: {
        path: 'avatar',
        type: 'string',
        length: {min: 8, max: 100},
    },
    text: {
        type: 'String',
        length: {min: 3, max:15},
    },
    longText: {
        type: 'String',
        length: {min: 3, max:250},
    },
    paragraph: {
        type: 'String',
        length: {min: 3, max:10000},
    },
    phone: {
        type: 'String',
        length: 13,
    },
    number: {
        type: 'Number',
        length: {min: 1, max:6},
    },
    capacity: {
        type: 'Number',
        path: 'capacity',
        length: {min: 1, max:3},
    },
    arrayOfStrings: {
        type: 'Array',
        items: {
            type: 'String',
            length: { min: 3, max: 100}
        }
    },
    obj: {
        type: 'Object',
    },
    bool: {
        type: 'Boolean',
    },
}