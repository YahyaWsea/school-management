const { dotEnv }  = require('../config/index.config');

module.exports = class MongoSeeder {
    constructor({ models, config }){
        this.models = models
        this.config = config
    }
    async seedSuperAdmin(){
        const seededSuperAdmin = await this.models.user.findOne({ username: "superadmin" });

        console.log('superAdmin', seededSuperAdmin)

        if (!seededSuperAdmin) {
          await this.models.user.create({
            username: dotEnv.SUPERADMIN_USERNAME,
            password: dotEnv.SUPERADMIN_PASSWORD,
            principalType: "superadmin"
          })
          console.log(`seeded superAdmin successfully`)
        }
    }

    async seedSchoolAdmin(){
      const seededSchoolAdmin1 = await this.models.user.findOne({ username: dotEnv.SCHOOLADMIN1_USERNAME });
      const seededSchoolAdmin2 = await this.models.user.findOne({ username: dotEnv.SCHOOLADMIN2_USERNAME });
      
      console.log({ sc2: dotEnv.SCHOOLADMIN2_USERNAME })

      if (!seededSchoolAdmin1) {
        await this.models.user.create({
          username: dotEnv.SCHOOLADMIN1_USERNAME,
          password: dotEnv.SCHOOLADMIN1_PASSWORD,
          principalType: "sc1"
        })
      }
      if (!seededSchoolAdmin2) {
        await this.models.user.create({
          username: dotEnv.SCHOOLADMIN2_USERNAME,
          password: dotEnv.SCHOOLADMIN2_PASSWORD,
          principalType: "sc2"
        })
      }

      console.log(`seeded schoolAdmin successfully`)
  }
}