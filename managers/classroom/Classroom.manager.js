const { SchemaTypes  } = require('mongoose');

module.exports = class ClassroomManager {

  constructor({ utils, cache, config, cortex, managers, validators, mongomodels } = {}) {
    this.config = config;
    this.validators = validators;
    this.mongomodels = mongomodels;
    this.httpExposed = ['create', 'get=find'];
  }

  async create({ __longToken, __authorizeSchoolAdminOnSchool, name, slug, description, capacity, schoolId }) {
    const classroomData = { name, slug, description, capacity, schoolId };

    // Data validation

    let result = await this.validators.classroom.create({ name, slug, schoolId });
    console.log({ result })
    if (result) return result;
    

    let classroom;
    try {
      const school = await this.mongomodels.school.findById(schoolId);
      if (!school) {
        return {
          error: "school not found"
        }
        
      }
      classroom = await this.mongomodels.classroom.create(classroomData);
      return {
        classroom,
      };
    } catch (error) {
      return {
        error
      }
    }
    
  }

  async find({__longToken, __query }) {

    console.log(__longToken)
    const { userKey } = __longToken;

    const assignedSchool = await this.mongomodels.school.findOne({ name: userKey });

    let classroom;
    if (__query) {
      classroom = await this.mongomodels.classroom.find({ ...__query, schoolId: assignedSchool?._id.toString() });
    } else {
      classroom = await this.mongomodels.classroom.find({ schoolId: assignedSchool?._id.toString() });
    }
    return {
      classroom
    };
  }


}