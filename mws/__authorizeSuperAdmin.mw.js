module.exports = ({ meta, config, managers, mongomodels }) =>{
  return ({ req, res, next })=>{
    const { userKey } = req.decoded;
    if (userKey !== 'superadmin') {
      return managers.responseDispatcher.dispatch(res, {ok: false, code:401, errors: 'you are not authorized to this route'});
    }
    next()
  }
}